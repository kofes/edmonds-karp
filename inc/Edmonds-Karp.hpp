#pragma once

#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <limits>

typedef long long value_type;

#pragma pack(push, 1)
class Graph {
public:
  Graph();
  ~Graph();
  void clear();
  value_type& capacity(size_t from, size_t to);
  value_type& flow(size_t from, size_t to);
  value_type max_flow();
private:
  //first - capacity; second - flow;
  std::vector< std::map<size_t, std::pair<value_type, value_type> > > _graph;
};
#pragma pack(pop)
