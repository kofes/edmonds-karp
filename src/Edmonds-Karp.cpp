#include "../inc/Edmonds-Karp.hpp"

Graph::Graph() {
  _graph.clear();
}
Graph::~Graph() {
  _graph.clear();
}
void Graph::clear() {
  _graph.clear();
}
value_type& Graph::capacity(size_t from, size_t to) {
  if (_graph.size() <= from || _graph.size() <= to)
    _graph.resize(std::max(from+1, to+1));
  return _graph[from][to].first;
}
value_type& Graph::flow(size_t from, size_t to) {
  if (_graph.size() <= from || _graph.size() <= to)
    _graph.resize(std::max(from+1, to+1));
  return _graph[from][to].second;
}
value_type Graph::max_flow() {
  value_type flow = 0;
  std::vector<value_type> prev;
  prev.resize(_graph.size());
  while (true) {
    std::queue<size_t> que;
    std::fill(prev.begin(), prev.end(), -1);
    que.push(0);
    while (!que.empty() && prev.back() < 0) {
      size_t v = que.front();
      que.pop();
      for (auto u : _graph[v])
        if (prev[u.first] < 0 && u.second.first > u.second.second) {
          prev[u.first] = v;
          que.push(u.first);
        }
    }
    if (prev.back() < 0)
      break;
    value_type min_edge_flow = std::numeric_limits<value_type>::max();
    for (size_t u = _graph.size()-1; u != 0; u = prev[u])
      min_edge_flow = std::min(min_edge_flow, _graph[prev[u]][u].first - _graph[prev[u]][u].second);
    for (size_t u = _graph.size()-1; u != 0; u = prev[u]) {
      _graph[prev[u]][u].second += min_edge_flow;
      _graph[u][prev[u]].second -= min_edge_flow;
    }
    flow += min_edge_flow;
  }
  return flow;
}
